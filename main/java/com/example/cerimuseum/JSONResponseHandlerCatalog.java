package com.example.cerimuseum;

import android.graphics.drawable.Drawable;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class parsing the input stream from the api, by getting it in a json reader and assigning all the values to the Museum Items by parsing this json reader.
 */
public class JSONResponseHandlerCatalog {
    /**
     * TAG for logcat
     */
    private static final String TAG = JSONResponseHandlerCatalog.class.getSimpleName();
    /**
     * List of museum items
     */
    private List<MuseumItem> museumItems;

    /**
     * Constructor to receive the list of MuseumItems to fill
     * @param museumItems
     */
    public JSONResponseHandlerCatalog(List<MuseumItem> museumItems) {
        this.museumItems = museumItems;
    }

    /**
     * Gets an input stream coming from the api, calls readCatalog for it to begin reading the root object.
     * @param response
     * @throws IOException
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }
    }

    /**
     * Gets the jsonreader coming from readJsonStream, "enters" in the root object, then calls the readCatalogItem for it to parse each object.
     * Each MuseumItem is initialized here, and added to the museumItems list holder.
     * @param reader
     * @throws IOException
     */
    public void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject(); // Root object
        while(reader.hasNext()) {
            MuseumItem mi = new MuseumItem("", // id
                    "", // Name
                    new ArrayList<String>(), // categories
                    "", // description
                    new ArrayList<String>(), // timeframe
                    null, // thumbnail
                    "", // year
                    "", // brand
                    new ArrayList<String>(), // technical details
                    false, // working
                    new HashMap<String, String>(), // stringPictures
                    new HashMap<String, Drawable>()); // realPictures
            String id = reader.nextName();
            mi.setId(id);
            readCatalogItem(reader, mi);
            museumItems.add(mi);
        }
        reader.endObject(); // End of root object
    }

    /**
     * Gets the current reader for it to go deeper by one object in the root and parse it. Assigns values to the MuseumItem being created up a function.
     * Goes on all the properties of each object the root contains, and through the arrays of them.
     * @param reader
     * @param mi
     * @throws IOException
     */
    public void readCatalogItem(JsonReader reader, MuseumItem mi) throws IOException {
        reader.beginObject(); // Begin scan object : Root Object > Object

        while (reader.hasNext()) { // each property of the current scanning object
            String name = reader.nextName();

            if(name.equals("name")) {
                mi.setName(reader.nextString());
            }

            else if (name.equals("description")) {
                mi.setDescription(reader.nextString());
            }

            else if (name.equals("timeFrame")) {
                reader.beginArray(); // TimeFrame array : Root Object > Object > Array
                while(reader.hasNext()) {
                    mi.getTimeFrame().add(String.valueOf(reader.nextInt()));
                }
                reader.endArray(); // End timeframe array
            }

            else if (name.equals("categories")) {
                reader.beginArray(); // The categories array : Root Object > Object > Array
                while(reader.hasNext()) {
                    mi.getCategories().add(reader.nextString());
                }
                reader.endArray(); // end categories array
            }

            else if (name.equals("technicalDetails")) {
                reader.beginArray();
                while(reader.hasNext()) {
                    mi.getTechnicalDetails().add(reader.nextString());
                }
                reader.endArray();
            }

            else if (name.equals("pictures")) {
                reader.beginObject(); // Pictures object : Root Object > Object > Object
                while(reader.hasNext()) {
                    mi.getStringPictures().put(reader.nextName(), reader.nextString());
                }
                reader.endObject(); // End Pictures object
            }

            else if (name.equals("brand")) {
                mi.setBrand(reader.nextString());
            }

            else if (name.equals("year")) {
                mi.setYear(String.valueOf(reader.nextInt()));
            }

            else if (name.equals("working")) {
                mi.setWorking(reader.nextBoolean());
            }
        }

        reader.endObject(); // end of current scanning object
    }
}
