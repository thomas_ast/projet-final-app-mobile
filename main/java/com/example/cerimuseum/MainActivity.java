package com.example.cerimuseum;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import java.util.List;

/**
 * Starting point of the app. Shows the entire catalog.
 */
public class MainActivity extends AppCompatActivity implements MuseumCatalogAdapter.OnMuseumItemListener {

    /**
     * The RecyclerView containing all the items of the Museum
     */
    private RecyclerView museumCatalog;
    /**
     * The adapter of the recyclerview
     */
    private MuseumCatalogAdapter catalogAdapter;
    /**
     * The layout manager of the Recycler view
     */
    private RecyclerView.LayoutManager catalogLayoutManager;
    /**
     * The list holding all Museum Items, filled by the data given by the api
     */
    private List<MuseumItem> catalog;


    /**
     * Sets the activity main view and its layout, then proceeds to call the class for a request of the catalog to the api
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        museumCatalog = findViewById(R.id.museum_catalog);

        catalogLayoutManager = new LinearLayoutManager(this);
        museumCatalog.setLayoutManager(catalogLayoutManager);

        MuseumDataApi mda = new MuseumDataApi(this);
        mda.execute();
    }

    /**
     * Sets the adapter for the recyclerview after receiving the data from the api through the parameters
     * @param museumItemList
     */
    public void updateRecyclerView(List<MuseumItem> museumItemList) {
        catalogAdapter = new MuseumCatalogAdapter(museumItemList, this);
        museumCatalog.setAdapter(catalogAdapter);
        this.catalog = museumItemList;
    }

    /**
     * Gets the item that is clicked, then sends its informations to MuseumItemActivity in a convenient way for extra of the intent going to MuseumItemActivity
     * @param position
     */
    @Override
    public void onMuseumItemClick(int position) {
        Intent museumItemInfo = new Intent(MainActivity.this, MuseumItemActivity.class);

        museumItemInfo.putExtra("id",               catalog.get(position).getId());
        museumItemInfo.putExtra("name",             catalog.get(position).getName());
        museumItemInfo.putExtra("categories",       catalog.get(position).getAllCategories());
        museumItemInfo.putExtra("description",      catalog.get(position).getDescription());
        museumItemInfo.putExtra("timeFrame",        catalog.get(position).getAllTimeFrames());
        museumItemInfo.putExtra("year",             catalog.get(position).getYear());
        museumItemInfo.putExtra("brand",            catalog.get(position).getBrand());
        museumItemInfo.putExtra("technicalDetails", catalog.get(position).getAllTechnicalDetails());
        museumItemInfo.putExtra("working",          catalog.get(position).getStringWorking());
        museumItemInfo.putExtra("stringPictures",   catalog.get(position).getStringPictures());

        startActivity(museumItemInfo);
    }
}
