package com.example.cerimuseum;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Binds the values for each row of museum items
 */
public class MuseumCatalogAdapter extends RecyclerView.Adapter<MuseumCatalogAdapter.MuseumCatalogViewHolder> {

    /**
     * List of all Museum Items for the adapter to read in and assign the values in the views
     */
    List<MuseumItem> museumItemList;
    /**
     * Listener of the click-on-museum-item handler implementation
     */
    private OnMuseumItemListener onMuseumItemListener;

    /**
     * Constructor for it to get the reference of the catalog of the Main Activity
     * @param museumItemList
     * @param onMuseumItemListener
     */
    public MuseumCatalogAdapter(List<MuseumItem> museumItemList, OnMuseumItemListener onMuseumItemListener) {
        this.museumItemList = museumItemList;
        this.onMuseumItemListener = onMuseumItemListener;
    }

    /**
     * Constructs and returns a MuseumCatalogViewHolder
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public MuseumCatalogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.museum_catalog_item, parent, false);
        return new MuseumCatalogViewHolder(view, onMuseumItemListener);
    }

    /**
     * Binds the values of the different elements of the layout (of the catalog items)
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull MuseumCatalogViewHolder holder, int position) {
        holder.itemName.setText(museumItemList.get(position).getName());
        holder.itemCategories.setText(museumItemList.get(position).getAllCategories());
        holder.itemBrand.setText(museumItemList.get(position).getBrand());
        holder.itemThumbnail.setImageDrawable(museumItemList.get(position).getThumbnail());
    }

    /**
     * Required for the binder to provide position in parameter
     * @return
     */
    @Override
    public int getItemCount() {
        return museumItemList.size();
    }

    /**
     * Nested class representing the view holders (each row) of the catalog
     */
    public class MuseumCatalogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * The name of the item displayed on its row
         */
        TextView itemName;
        /**
         * The categories of the item displayed on its row
         */
        TextView itemCategories;
        /**
         * The brand of the item displayed on its row
         */
        TextView itemBrand;
        /**
         * The thumbnail of the item displayed on its row
         */
        ImageView itemThumbnail;
        /**
         * The listener for this item
         */
        OnMuseumItemListener onMuseumItemListener;

        /**
         * Constructors to retrieve and bind each element on the layout
         * @param itemView
         * @param onMuseumItemListener
         */
        public MuseumCatalogViewHolder(@NonNull View itemView, OnMuseumItemListener onMuseumItemListener) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_name);
            itemCategories = itemView.findViewById(R.id.item_categories);
            itemBrand = itemView.findViewById(R.id.item_brand);
            itemThumbnail = itemView.findViewById(R.id.item_thumbnail);
            this.onMuseumItemListener = onMuseumItemListener;

            itemView.setOnClickListener(this);
        }

        /**
         * onClick handler aliasing the onMuseumItemClick implementation
         * @param v
         */
        @Override
        public void onClick(View v) {
            onMuseumItemListener.onMuseumItemClick(getAdapterPosition());
        }
    }

    /**
     * Interface for redefining our own implementation of what happens when we click
     */
    public interface OnMuseumItemListener {
        void onMuseumItemClick(int position);
    }

}
