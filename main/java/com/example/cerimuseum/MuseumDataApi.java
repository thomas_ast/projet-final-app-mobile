package com.example.cerimuseum;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that handles the api input stream of the catalog and gives it to the jsonResponseHandler.
 * Does it into a separated thread in doInBackGround.
 */
public class MuseumDataApi extends AsyncTask<Void,Void,List<MuseumItem>> {

    /**
     * TAG for Logcat
     */
    private static final String TAG = MuseumDataApi.class.getSimpleName();

    /**
     * Reference of the activity to permit the call of updateRecyclerView
     */
    private Activity activity;


    /**
     * Constructor to get the main activity reference
     * @param activity
     */
    public MuseumDataApi(Activity activity) {
        this.activity = activity;
    }

    /**
     * Puts a temporary message on screen to inform of the current data retrieving
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Toast toast = Toast.makeText(activity.getApplicationContext(), "Chargement des données, veuillez patienter...", Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Seperated thread to retrieve the informations coming from the api in the background.
     * Also builds a url to retrieve each thumbnail of the catalog's items, itself given to onPostExecute
     * @param voids
     * @return
     */
    @Override
    protected List<MuseumItem> doInBackground(Void... voids) {
        URL url;
        HttpURLConnection urlConnection = null;
        List<MuseumItem> museumItems = new ArrayList<MuseumItem>(); // The catalog, with all the items
        JSONResponseHandlerCatalog jsonResponseHandlerCatalog = new JSONResponseHandlerCatalog(museumItems);

        try {
            // Fetching and building all the catalog items
            url = WebServiceUrl.buildSearchCatalog();
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = urlConnection.getInputStream();
            jsonResponseHandlerCatalog.readJsonStream(in);
            in.close();

            // Fetching and building all the items thumbnails then putting them in the catalog
            for(int i = 0; i < museumItems.size(); i++) {
                url = WebServiceUrl.buildSearchThumbnail(museumItems.get(i).getId());
                urlConnection = (HttpURLConnection) url.openConnection();
                in = urlConnection.getInputStream();

                String thumbnail = "";
                Drawable d = Drawable.createFromStream(in, thumbnail);
                museumItems.get(i).setThumbnail(d);

                in.close();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return museumItems;
    }

    /**
     * Calls for assigning the adapter on the recycler view and show the catalog
     * @param result
     */
    @Override
    protected void onPostExecute(List<MuseumItem> result) {
        ((MainActivity) activity).updateRecyclerView(result);
    }

}
