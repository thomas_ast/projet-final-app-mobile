package com.example.cerimuseum;

import android.graphics.drawable.Drawable;

import java.util.HashMap;
import java.util.List;

/**
 * Represents an item of the museum
 */
public class MuseumItem {
    /**
     * Item's ID
     */
    private String id;
    /**
     * Item's name
     */
    private String name;
    /**
     * Item's categories
     */
    private List<String> categories;
    /**
     * Item's description, can be not set
     */
    private String description;
    /**
     * Item's time frame
     */
    private List<String> timeFrame;

    /**
     * Item's thumbnail picture, can be blank
     */
    private Drawable thumbnail;

    /**
     * Item's year, can be not set
     */
    private String year;
    /**
     * Item's brand, can be not set
     */
    private String brand;
    /**
     * Item's technical details, can be not set
     */
    private List<String> technicalDetails;
    /**
     * Item's condition
     */
    private boolean working;
    /**
     * Dictionary of image ID => image Description
     */
    private HashMap<String, String> stringPictures;
    /**
     * Dictionary of image ID => real drawable picture
     */
    private HashMap<String, Drawable> drawablePictures;


    /**
     * Constructor to instantiate an item
     * @param id
     * @param name
     * @param categories
     * @param description
     * @param timeFrame
     * @param thumbnail
     * @param year
     * @param brand
     * @param technicalDetails
     * @param working
     * @param stringPictures
     * @param drawablePictures
     */
    public MuseumItem(String id, String name, List<String> categories, String description, List<String> timeFrame, Drawable thumbnail, String year, String brand, List<String> technicalDetails, boolean working, HashMap<String, String> stringPictures, HashMap<String, Drawable> drawablePictures) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.thumbnail = thumbnail;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.stringPictures = stringPictures;
        this.drawablePictures = drawablePictures;
    }

    /**
     * Gives the item's id
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the item's id
     * @param id
     */
    public void setId(String id) {
         this.id = id;
    }

    /**
     * Gets the item's name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the item's name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gives the item's list  of categories
     * @return
     */
    public List<String> getCategories() {
        return categories;
    }

    /**
     * Gives all of the item's categories, in a single string with carriers return for it to be displayed on the Item info's activity
     * @return
     */
    public String getAllCategories() {
        String s = "";
        for(int i = 0; i < categories.size(); i++) {
            s = s + " " + categories.get(i);
        }
        return s;
    }

    /**
     * Sets the item's list of categories
     * @param categories
     */
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    /**
     * Gets the item's description
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the item's description
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the item's list of time frame
     * @return
     */
    public List<String> getTimeFrame() {
        return timeFrame;
    }

    /**
     * Gets all item's time frames in a single string for it to be displayed on the Item info's activity
     * @return
     */
    public String getAllTimeFrames() {
        String s = "";
        for(String t : timeFrame) {
           s = s + String.valueOf(t) + "\n";
        }
        return s;
    }

    /**
     * Sets item's time frame
     * @param timeFrame
     */
    public void setTimeFrame(List<String> timeFrame) {
        this.timeFrame = timeFrame;
    }

    /**
     * Gets item's thumbnail picture
     * @return
     */
    public Drawable getThumbnail() {
        return thumbnail;
    }

    /**
     * Sets item's thumbnail
     * @param thumbnail
     */
    public void setThumbnail(Drawable thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * Gets item's year
     * @return
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets item's year
     * @param year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * Gets item's brand
     * @return
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets item's brand
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * Gets item's list of technical details
     * @return
     */
    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    /**
     * Gets all of item's technical details in a single string for it to be displayed on the Item info's activity
     * @return
     */
    public String getAllTechnicalDetails() {
        String s = "";
        for(String t : technicalDetails) {
            if(!t.equals("")) {
                s = s + t + "\n";
            }
        }
        return s;
    }

    /**
     * Sets the item's technical details
     * @param technicalDetails
     */
    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    /**
     * Gets a string of the item's condition
     * @return
     */
    public String getStringWorking() {
        return String.valueOf(working);
    }

    /**
     * Sets the item's condition
     * @param working
     */
    public void setWorking(boolean working) {
        this.working = working;
    }

    /**
     * Gets the item's dictionary image ID => image Description
     * @return
     */
     public HashMap<String, String> getStringPictures() {
        return stringPictures;
     }

    /**
     * Sets the item's dictionary image ID => image Description
     */
    public void setStringPictures() {
        this.stringPictures = stringPictures;
     }

    /**
     * Gets the item's dictionary image ID => real drawable image
      * @return
     */
    public HashMap<String, Drawable> getDrawablePictures() {
        return drawablePictures;
    }

    /**
     * Sets the item's dictionary image ID => real drawable image
     */
    public void setDrawablePictures() {
        this.drawablePictures= drawablePictures;
    }
}
