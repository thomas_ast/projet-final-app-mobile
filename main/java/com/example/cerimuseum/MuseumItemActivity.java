package com.example.cerimuseum;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The item info screen
 */
public class MuseumItemActivity extends AppCompatActivity {

    /**
     * TAG for Logcat
     */
    private static final String TAG = MuseumItemActivity.class.getSimpleName();

    /**
     * Item's name text
     */
    private TextView textItemName;
    /**
     * Item's categories text
     */
    private TextView textItemCategories;
    /**
     * Item's description text
     */
    private TextView textItemDescription;
    /**
     * Item's time frame text
     */
    private TextView textItemTimeFrame;


    /**
     * Item's year text
     */
    private TextView textItemYear;
    /**
     * Item's brand text
     */
    private TextView textItemBrand;
    /**
     * Item's technical details text
     */
    private TextView textItemTechnicalDetails;
    /**
     * Item's condition text
     */
    private TextView textItemWorking;

    /**
     * Item's id
     */
    private String id;
    /**
     * Item's image ID => image Description dictionary
     */
    private HashMap<String, String> stringPictures;

    // Gallery
    /**
     * Recycler view of the item's pictures gallery
     */
    private RecyclerView museumItemGallery;
    /**
     * Adapter of the item's pictures gallery
     */
    private MuseumItemGalleryAdapter museumItemGalleryAdapter;
    /**
     * LayoutManager of of the item's pictures gallery
     */
    private RecyclerView.LayoutManager museumItemGalleryLayoutManager;

    /**
     * Array used to store all pictures drawables in a convenient way to be displayed later on
     */
    private List<Drawable> picturesDrawable = new ArrayList<>();
    /**
     * Array used to store all pictures descriptions in a convenient way to be displayed later on
     */
    private List<String> picturesDescription = new ArrayList<>();


    /**
     * Binds the elements of the screen then reads the extras of the intent received by Main Activity, corrects future string to be displayed if they're null.
     * Finally fills the recycler view gallery
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum_item);

        textItemName = findViewById(R.id.museum_item_name);
        textItemCategories = findViewById(R.id.museum_item_categories);
        textItemDescription = findViewById(R.id.museum_item_description);
        textItemTimeFrame = findViewById(R.id.museum_item_timeframe);
        textItemYear = findViewById(R.id.museum_item_year);
        textItemBrand = findViewById(R.id.museum_item_brand);
        textItemTechnicalDetails = findViewById(R.id.museum_item_technicalsDetails);
        textItemWorking = findViewById(R.id.museum_item_working);


        textItemName.setText(getIntent().getStringExtra("name"));

        textItemCategories.setText(getIntent().getStringExtra("categories"));

        if(getIntent().getStringExtra("description").equals("")) {
            textItemDescription.setText("Aucune description");
        } else {
            textItemDescription.setText(getIntent().getStringExtra("description"));
        }

        textItemTimeFrame.setText(getIntent().getStringExtra("timeFrame"));

        if(getIntent().getStringExtra("year").equals("")) {
            textItemYear.setText("Aucune année");
        } else {
            textItemYear.setText(getIntent().getStringExtra("year"));
        }

        if(getIntent().getStringExtra("brand").equals("")) {
            textItemBrand.setText("Aucune marque");
        } else {
            textItemBrand.setText(getIntent().getStringExtra("brand"));
        }

        if(getIntent().getStringExtra("technicalDetails").equals("")) {
            textItemTechnicalDetails.setText("Aucun détails techniques");
        } else {
            textItemTechnicalDetails.setText(getIntent().getStringExtra("technicalDetails"));
        }

        if(getIntent().getStringExtra("working").equals("")) {
            textItemWorking.setText("Aucune information sur son fonctionnement");
        } else if(getIntent().getStringExtra("working").equals("true")){
            textItemWorking.setText("Oui");
        } else if (getIntent().getStringExtra("working").equals("false")) {
            textItemWorking.setText("Ne fonctionne pas");
        } else {
            textItemWorking.setText(getIntent().getStringExtra("working"));
        }


        // Gallery part
        id = getIntent().getStringExtra("id");
        stringPictures = (HashMap<String, String>) getIntent().getSerializableExtra("stringPictures"); // Image ID, Image description

        if(!stringPictures.isEmpty()) {
            MuseumDataApiImageGetter mdapig = new MuseumDataApiImageGetter();
            mdapig.execute();

            museumItemGallery = findViewById(R.id.museum_item_gallery);
            museumItemGalleryLayoutManager = new LinearLayoutManager(this);
            museumItemGalleryAdapter = new MuseumItemGalleryAdapter(this, picturesDrawable, picturesDescription);
            museumItemGallery.setLayoutManager(museumItemGalleryLayoutManager);
            museumItemGallery.setAdapter(museumItemGalleryAdapter);
        }

    }

    /**
     * Object used as an convenient intermediate for values returned after the search for an image by doInBackground
     */
    public class PictureIdDrawable {
        /**
         * Item's image id
         */
        String id;
        /**
         * Item's drawable id
         */
        Drawable d;

        /**
         * Object constructor
         * @param id
         * @param d
         */
        public PictureIdDrawable(String id, Drawable d) {
            this.id = id;
            this.d = d;
        }

        /**
         * Gets the intermediate object id hold
         * @return
         */
        public String getId() {
            return id;
        }

        /**
         * Gets the intermediate object drawable hold
         * @return
         */
        public Drawable getD() {
            return d;
        }
    }


    /**
     * Nested class to get the needed image from the api
     */
    public class MuseumDataApiImageGetter extends AsyncTask<Void, Void, PictureIdDrawable> {

        /**
         * TAG for Logcat
         */
        public final String TAG = MuseumDataApiImageGetter.class.getSimpleName();

        /**
         * Shows a message to inform that images are being retrieved
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast toast = Toast.makeText(getApplicationContext(), "Chargement des images, veuillez patienter...", Toast.LENGTH_SHORT);
            toast.show();
        }

        /**
         * Gets and returns all images of the items by requesting them to the api. Creates Drawables with it.
         * @param voids
         * @return
         */
        @Override
        protected PictureIdDrawable doInBackground(Void... voids) {
            URL url;
            HttpURLConnection urlConnection = null;
            InputStream in;

            try {
                // Get all images of the item in drawablePictures
                for (Map.Entry<String, String> entry : stringPictures.entrySet()) {
                    url = WebServiceUrl.buildSearchPicture(id, entry.getKey()); // Reach for /items/<itemID>/images/<imageID>, with itemID = id and imageID = entry.getKey()
                    urlConnection = (HttpURLConnection) url.openConnection();
                    in = urlConnection.getInputStream();

                    String picture = "";
                    Drawable d = Drawable.createFromStream(in, picture);

                    PictureIdDrawable pictureIdDrawable = new PictureIdDrawable(entry.getValue(), d);

                    in.close();

                    return pictureIdDrawable;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * Drawables made and pictures id are added to the convenient arrays for the display
         * @param result
         */
        @Override
        protected void onPostExecute(PictureIdDrawable result) {
            picturesDrawable.add(result.getD());
            picturesDescription.add(result.getId());
        }

    }

}
