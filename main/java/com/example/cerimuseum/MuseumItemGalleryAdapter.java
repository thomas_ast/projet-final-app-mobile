package com.example.cerimuseum;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Binds the values for each row of the gallery
 */
public class MuseumItemGalleryAdapter extends RecyclerView.Adapter<MuseumItemGalleryAdapter.MuseumItemGalleryViewHolder> {

    /**
     * Convenient array to get drawable of pictures and display them
     */
    List<Drawable> picturesDrawable;
    /**
     * Convenient array to get pictures description and display them
     */
    List<String> picturesDescription;
    /**
     * The concerned context
     */
    Context context;

    /**
     * Constructor for it to reference picturesDrawable and picturesDescription of the MuseumItemActivity
     * @param context
     * @param picturesDrawable
     * @param picturesDescription
     */
    public MuseumItemGalleryAdapter(Context context, List<Drawable> picturesDrawable, List<String> picturesDescription) {
        this.context = context;
        this.picturesDrawable = picturesDrawable;
        this.picturesDescription = picturesDescription;
    }

    /**
     * Constructs and returns a MuseumItemGalleryViewHolder
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public MuseumItemGalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.museum_item_picture, parent, false);
        return new MuseumItemGalleryViewHolder(view);
    }

    /**
     * Binds the values of the different elements of the layout (of the gallery items pictures)
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull MuseumItemGalleryViewHolder holder, int position) {
        holder.pictureDrawable.setImageDrawable(picturesDrawable.get(position));
        holder.pictureDescription.setText(picturesDescription.get(position));
    }

    /**
     * Required for the binder to provide position in parameter
     * @return
     */
    @Override
    public int getItemCount() {
        return picturesDrawable.size();
    }

    /**
     * Nested class representing the view holders (each row) of the item picture in the gallery
     */
    public class MuseumItemGalleryViewHolder extends RecyclerView.ViewHolder {

        /**
         * The picture element displayed
         */
        ImageView pictureDrawable;
        /**
         * The discrition text element displayed
         */
        TextView pictureDescription;

        /**
         * Constructors to retrieve and bind each element on the layout
         * @param itemView
         */
        public MuseumItemGalleryViewHolder(@NonNull View itemView) {
            super(itemView);
            picturesDrawable = itemView.findViewById(R.id.museum_item_gallery_picture);
            pictureDescription = itemView.findViewById(R.id.museum_item_gallery_picture_description);
        }

    }

}