package com.example.cerimuseum;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Gets the needed URL for api requests
 */
public class WebServiceUrl {

    /**
     * Base name of the website
     */
    private static final String HOST = "demo-lia.univ-avignon.fr";
    /**
     * The first subpath
     */
    private static final String PATH_1 = "cerimuseum";

    /**
     * Builds the common part of all requests urls
     * Gives : https://demo-lia.univ-avignon.fr/cerimuseum/
     * @return
     */
    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    /**
     * String for the subpath to look for ids
     */
    private static final String SEARCH_IDS = "ids";
    /**
     * Adds up subpath to look for ids
     * Gives : https://demo-lia.univ-avignon.fr/cerimuseum/ids
     */
    public static URL buildSearchIds() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    /**
     * String for the subpath to look for the catalog
     */
    private static final String SEARCH_CATALOG = "catalog";
    /**
     * Adds up subpath to look for the catalog
     * Gives : https://demo-lia.univ-avignon.fr/cerimuseum/catalog
     */
    public static URL buildSearchCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    /**
     * String for the subpath to look for the items
     */
    private static final String SEARCH_ITEMS = "items";

    /**
     * Adds up subpath to look for the thumbnail of the item
     * Gives : https://demo-lia.univ-avignon.fr/cerimuseum/items/<itemID>/thumbnail/
     */
    private static final String SEARCH_THUMBNAIL = "thumbnail";
    public static URL buildSearchThumbnail(String itemID) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(itemID)
                .appendPath(SEARCH_THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    /**
     * String for the subpath to look for the images of the items
     */
    private static final String SEARCH_IMAGES = "images";
    /**
     * Adds up subpath to look for single particular images of the items
     * Gives : https://demo-lia.univ-avignon.fr/cerimuseum/items/<itemID>/images/<imageID>/
     * @param itemID
     * @param imageID
     * @return
     * @throws MalformedURLException
     */
    public static URL buildSearchPicture(String itemID, String imageID) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(itemID)
                .appendPath(SEARCH_IMAGES)
                .appendPath(imageID);
        URL url = new URL(builder.build().toString());
        return url;
    }

}
